/*PASSO 1: Criar as variáveis e os elementos via DOM, fazendo uma váriavel que chame todas as varetas por classname;

PASSO 2: Fazer um loop que percorra por dentro do classname das vareta e uma função que tenha um evento;

PASSO 3: Criar uma condicional para diferenciar se a variável handDisk está vazia ou se contem algum objeto;

PASSO 4: Criar a condição de mover uma peça somente se a vareta tiver um disco maior ou estiver vazia;

PASSO 5: Criar a condição de vitória de que quando os discos estiverem empilhados na ordem certa o player terá vencido.*/

//Variáveis e elementos:

const sticks = document.getElementsByClassName("sticks");
const disk1 = document.createElement("div");
const disk2 = document.createElement("div");
const disk3 = document.createElement("div");
const disk4 = document.createElement("div");
const stick1 = document.getElementById("start");

disk1.id = "disco1";
disk2.id = "disco2";
disk3.id = "disco3";
disk4.id = "disco4";

stick1.appendChild(disk1);
stick1.appendChild(disk2);
stick1.appendChild(disk3);
stick1.appendChild(disk4);

let qtd = document.querySelectorAll("#start div").length;
let handDisk = null;
let cssDisk;
let lastElementI;

//Loop, função e condicionais:
for (let i of sticks) {
  i.addEventListener("click", moveDisk);
}
function moveDisk(event) {
  if (event.currentTarget.lastElementChild !== null && handDisk === null) {
    handDisk = event.currentTarget.lastElementChild;
  } else if (handDisk !== null) {
    let handDiskWidth = handDisk.getBoundingClientRect().width;
    let disk = event.currentTarget.lastElementChild;
    let diskWidth = 0;
    if (disk !== null) {
      diskWidth = disk.getBoundingClientRect().width;
    }
    if (disk === null || diskWidth > handDiskWidth) {
      event.currentTarget.appendChild(handDisk);
      console.log(event.currentTarget);
    }
    let qtd = document.querySelectorAll("#end div").length;
    console.log(qtd);
    if (qtd == 4) {
      return mostraVitoria();
    }
    handDisk = null;
  }
}

function mostraVitoria(){
    const mensagemVitoria = document.getElementById("vitoria")
    mensagemVitoria.classList.remove("hidden")
   
}
